package pro.sdacademy.gl.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pro.sdacademy.gl.spring.beans.TextFileParserRunner;

public class MySpringApplication {

    public static void main(String[] args) {
        //BeanFactory
        //AnnotationConfigApplicationContext
        ApplicationContext context = new AnnotationConfigApplicationContext("pro.sdacademy.gl.spring");

        TextFileParserRunner runner =  context.getBean(TextFileParserRunner.class);
        runner.run();
    }
}
