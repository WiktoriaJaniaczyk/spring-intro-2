package pro.sdacademy.gl.spring.beans;

import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;

@Component
@AllArgsConstructor
public class TextFileParser {

    private final FileLineValidator validator;

    //korzystamy z gotowej metody zamiast 2 linijek kodu
    public void parse(Resource file){
        //raz bardzo duzy plik, to strumieniujemy
        //po linijce
        try (var stream = Files.newInputStream(file.getFile().getAbsoluteFile().toPath());
             var reader = new BufferedReader(new InputStreamReader(stream))){

            reader.lines()
                    .filter(line -> this.validator.validate(line))
                    .forEach(line -> System.out.println("Line is correct: " + line));

        } catch (IOException e) {
            e.printStackTrace();
        }
        //maly plik, wczytuje caly plik do pamieci
        //zwraca tablice bajtow (zera i jedynki) mozna to
        //przeksztalcic na stringa
//        Files.readAllBytes(file.getFile().getPath());

    }
}
