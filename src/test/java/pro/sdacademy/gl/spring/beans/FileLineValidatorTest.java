package pro.sdacademy.gl.spring.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileLineValidatorTest {

//    private FileLineValidatorConfiguration configurationion = new FileLineValidatorConfiguration();
    private FileLineValidatorConfiguration configuration;
    FileLineValidator validator;

    //jesli testy przejda to trzeba je zepsuc

    @BeforeEach
    void setUp() {
        //tu nie ma mowy o pliku application properites
        configuration = Mockito.mock(FileLineValidatorConfiguration.class);
        Mockito.when(configuration.getCorrectWords()).thenReturn(List.of("Audi"));
        validator = new FileLineValidator(configuration);
    }

    @Test
    void shouldValidateCorrectly() {
        // when
        var result = this.validator.validate("'An Audi is a very super car.");

        // then
        assertTrue(result);
    }

    @Test
    void shouldValidateIncorrectly() {
        // when
        var result = this.validator.validate("'A BMW is a very super car.");

        // then
        assertFalse(result);
    }
}